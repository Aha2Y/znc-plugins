/*
 * IRCbot "Infobot" module for ZNC.
 * Made by Aha2Y.
*/

#include <znc/znc.h>
#include <znc/Client.h>
#include <znc/Chan.h>
#include <znc/User.h>
#include <znc/Modules.h>

string replace(string orig, string search, string repl)
{
    string rep = orig;
    size_t f = rep.find(search);
    while (f != string::npos)
    {  
        rep.replace(f, search.length(), repl);
        f = rep.find(search, f + repl.length() + 1 + 1);
    }  
    return rep;
}

//Start code of InfoBot
class CInfoBot : public CModule {

public:
	MODCONSTRUCTOR(CInfoBot) {}
   
	virtual EModRet OnChanMsg(CNick& Nick, CChan& Channel, CString& sMessage) 
   {   
		if (sMessage == "?ping") 
      {
          PutIRC("PRIVMSG " + Channel.GetName() + " :" + Nick.GetNick() + ": Pong!");
      
          return CONTINUE;
		}
      
      else if (sMessage == "?uptime") 
      {
          PutIRC("PRIVMSG " + Channel.GetName() + " :The bouncer has been up for: " + replace(CZNC::Get().GetUptime(), "d", " days"));
          
          return CONTINUE;
      }
      
      else if (sMessage == "?version") 
      {
          PutIRC("PRIVMSG " + Channel.GetName() + " :" + Nick.GetNick() + ": InfoBot, Running on ZNC-" + CZNC::Get().GetVersion());
          
          return CONTINUE;
          
      }
   }
};

template<> void TModInfo<CInfoBot>(CModInfo& Info) {
	Info.SetWikiPage("Infobot");
}

USERMODULEDEFS(CInfoBot, "Infobot by Aha2Y (BETA)")
